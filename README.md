# MainPage
Main page with App summary, download and additional info

> Humus is a fertile ground for creatives, makers, artists, entrepreneurs and professionals from all sectors to share ideas, receive feedback and find collaborators with whom to implement them.

Published page
```sh
https://cristianambrosini.gitlab.io/humus
```

# Terms & Conditions
Terms & Conditions legal agreements between a Humus service provider and a person who wants to use that service.

> This Terms and Conditions set the way in which the App and the whole service must be used, in a legally binding way. It is crucial for protecting the content from a copyright perspective as well as for protecting from potential liabilities.

Published page
```sh
https://cristianambrosini.gitlab.io/humus/terms-conditions/
```

# Privacy Policy
Privacy Policy to comply with legislation around the world.

> This Privacy Policy is meant to help you understand what information we collect and why we collect it.

Published page
```sh
https://cristianambrosini.gitlab.io/humus/privacy-policy
```

# Remove Informations
Remove informations procedure to deactivate your account and delete any information we store about the user.

> This Remove Informations procedure is meant to help you easyly understand how you can delete your information.

Published page
```sh
https://cristianambrosini.gitlab.io/humus/remove-informations
```

## Licensed projects

- Humus App Android 
- Humus App iOS 
